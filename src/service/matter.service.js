import { QueryBuilderService } from "./sql/querybuilder.service";
import { SqlService } from "./sql/sql.service";
import { dbview, table } from "../enum/table";
import * as _ from 'lodash';
import Utils from "./common/utils";
import { AppCode } from "../enum/app-code";
import { ErrorModel } from "../model/common.model";
import { Environment, Message, ProfileType } from "../enum/common.enum";
import Validator from "./common/validator.service";
import { Config } from "../config";
import {settings} from "../enum/common.enum";

export class MatterService {
    constructor() {

    }

    //used function
    async listAccount(matter){
        const query = `select a.* from ${table.accounts} a order by id asc
        LIMIT ${matter.limit || 10} 
        OFFSET ${matter.offset || 0};`;
        return await SqlService.executeQuery(query);
    }

    async listBank(matter){
        const query = `select b.* from ${table.bank} b where b.ac_id = '${matter.id}' order by b.bank_id asc`;
        return await SqlService.executeQuery(query);
    }

    async listWeather(matter){
        const query = `select w.* from ${table.weather} w inner join ${table.bank} b on w.w_id=b.w_id   where b.bank_id = '${matter.bank_id}' order by w.w_id asc`;
        return await SqlService.executeQuery(query);
    }


    async createMatter(matter) {
        matter.createdAt = "utc_timestamp()";
        const query = QueryBuilderService.getInsertQuery(table.matter, matter);
        return SqlService.executeQuery(query);
    }

    async tagMatters(matter){
        matter.createdAt = "utc_timestamp()";
        const query = QueryBuilderService.getInsertQuery(table.tagged_matters, matter);
        return SqlService.executeQuery(query);
    }



}
