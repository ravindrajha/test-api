import express from 'express';
import { HttpCode } from "../enum/http-code";
import { AppCode } from "../enum/app-code";
import { MatterService } from "../service/matter.service";
import { MatterController } from "../controller//matter.controller";
import AppOverrides from "../service/common/app.overrides";
import { validateAuthToken } from "../middleware/auth.middleware";
import { log } from "../service/common/logger.service";
import * as _ from 'lodash';

const router = express();

export class MatterRoutes {
    constructor(app) {
        new AppOverrides(router);
        app.use('/matter', router);
        this.matterController = new MatterController();
        this.matterService = new MatterService();
        this.initRoutes();
    }
    initRoutes() {
        router.use(validateAuthToken);
        

        router.post('/list-data-acc', async (req, res) => {
            try {
				//console.log(req.user);
                let praGetData = {
                    limit: req.body.limit,
                    offset: req.body.page * 10,                                      
                };
                
                let accounts = await this.matterService.listAccount(praGetData);
                if (_.isEmpty(accounts)) {
                    return res.json({
                        status: HttpCode.ok,
                        message: "No record found",
                        data: accounts,
                        total_records: 0
                    });
                }
                
                let banks=[];
				for (const key in accounts) { 
					banks = await this.matterService.listBank(accounts[key].id);
					accounts[key].banks=banks;
                    //accounts[key].associated_member = await this.contactService.getAssignedMember(accounts[key].id);
				}

                for (const key in banks) { 
					banks[key].weather = await this.matterService.listBank(banks[key].bank_id);
					
				}

                return res.json({
                        status: HttpCode.ok,
                        message: "Contact List",
                        data: accounts,
                        //total_records: await this.contactService.getAllContactCount(praGetData)
                    });


            } catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                if (e.code === AppCode.invalid_creds) {
                    return res.status(HttpCode.unauthorized).send(e);
                }
                res.sendStatus(HttpCode.internal_server_error);
            }
        });
        
        

        

    }
}
