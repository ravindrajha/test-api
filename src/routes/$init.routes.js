import {HttpCode} from "../enum/http-code";
import {SqlService} from "../service/sql/sql.service";
import Utils from "../service/common/utils";
import { Config } from "../config";
import { log } from "../service/common/logger.service";
import { MatterRoutes } from './matter.routes';
export class InitRoutes {

    constructor(app) {
        new SqlService();

        this.initTestApi(app);
        this.initRoutes(app);
    }

    initTestApi(app) {
        app.get('/', async (req, res) => {
            return res.json({
                version: Utils.getVersion(),
                system_time: new Date(),
                env: process.env.NODE_ENV
            });
        });

        app.get('/db', async (req, res) => {
            try {
                await SqlService.getTable('user');
                return res.json({
                    message: 'database working'
                });
            } catch (e) {
                log.e(`${req.method}: ${req.url}`, e);
                return res.status(HttpCode.internal_server_error).json(e);
            }
        });
    }
    initRoutes(app) {
        
        new MatterRoutes(app);
        
    }
}
