import * as _ from 'lodash';
import { Config } from '../config'
import { AppCode } from "../enum/app-code";
import { MatterService } from "../service/matter.service";
import Validator from "../service/common/validator.service";
import jwt from "jsonwebtoken";
import { HttpCode } from "../enum/http-code";
import { AgeRange } from "../enum/common.enum";
import Utils from "../service/common/utils";
import { log } from "../service/common/logger.service";
import { table } from "../enum/table";
import { SqlService } from "../service/sql/sql.service";

export class MatterController {
    constructor() {
        this.matterService = new MatterService();
        
    }

    
}