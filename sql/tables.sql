--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `name`) VALUES
(1, 'Jane Doe');

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `ac_id` bigint(20) DEFAULT NULL,
  `bank_id` bigint(20) NOT NULL,
  `ifsc` varchar(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `branch` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `w_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`ac_id`, `bank_id`, `ifsc`, `name`, `branch`, `city`, `w_id`) VALUES
(1, 1, 'HDFC0001758', 'HDFC Bank', 'IRR, KORAMANGALA, BANGALORE', 'BANGALORE', 1),
(1, 2, 'HDFC0003933', 'HDFC Bank', 'AKCHA KUSHMANDI', 'KUSHMANDI', 2);

-- --------------------------------------------------------

--
-- Table structure for table `weather`
--

CREATE TABLE `weather` (
  `w_id` bigint(20) NOT NULL,
  `temperature` varchar(20) DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `min` smallint(2) DEFAULT NULL,
  `max` smallint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `weather`
--